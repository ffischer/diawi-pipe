#!/usr/bin/env bash
#
# This pipe deploys your android-artifact to the diawi and outputs the customised diawi-download url. A free diawi-account is mandantory for this pipe (to get an api-diawi-upload token). Diawi is an public android-/iOS-Store which is commonly used for testing-purpose.
#

source "$(dirname "$0")/common.sh"
#https://dashboard.diawi.com/docs/apis/upload
info "Executing the pipe..."

# Required parameters
FILE=${FILE:?'FILE path to android artifact is missing.'}
DIAWI_TOKEN=${DIAWI_TOKEN:?'DIAWI_TOKEN is missing.'}
CALLBACK_EMAIL=${CALLBACK_EMAIL:?'CALLBACK_EMAIL is missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}
WALL_OF_APPS=${WALL_OF_APPS:=0}

# Validate parameters
## FILE
if [ ! -f "$FILE" ]; then
    fail "$FILE does not exist"
fi
##CALLBACK_EMAIL
if [[ $CALLBACK_EMAIL != *"@"* ]]; then
  fail "${CALLBACK_EMAIL} --> It's not a valid email address"
fi


output="$(curl https://upload.diawi.com/ -s -F token=${DIAWI_TOKEN} -F file=@${FILE} -F find_by_udid=1 -F wall_of_apps=$WALL_OF_APPS -F callback_email=${CALLBACK_EMAIL})"

if [[ "${status}" == "0" ]]; then
  echo "${output}"
  success "Success!"
else
  fail "Error: ${output}"
fi
