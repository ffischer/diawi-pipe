[![buy me a coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png)](https://www.buymeacoffee.com/CWkjUYH)

# Bitbucket Pipelines Pipe: Diawi-Android Pipe

This pipe deploys your android-artifact to the diawi store and outputs the customised diawi-download url. A free diawi-account is mandantory for this pipe (to get an api-diawi-upload token). Diawi is an public android-/iOS-Store which is commonly used for testing-purpose.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: ffischer/diawi-pipe:0.1.2
    variables:
      FILE: "<string>"
      DIAWI_TOKEN: "<string>"
```
## Variables

| Variable        | Type     | Usage                                             |          
| --------------- | --------|--------------------------------------------------- |
| FILE (*)        | String | location of the file. e.g `/SampleAndroidApp/build/outputs/apk/debug/SampleAndroidApp.apk` |
| DIAWI_TOKEN (*) | String | Has to be generated from Diawi. [Generate token](https://dashboard.diawi.com/profile/api) |
| WALL_OF_APPS    | INT    | Number of 0 or 1. 1: App will be publicly shown on the diawi-website for everybody. 0: App will be hidden |
| CALLBACK_EMAIL(*)  | String | E-Mail to send the diawi-download-url |


_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: ffischer/diawi-pipe:0.1.2
    variables:
      NAME: "foobar"
```

Advanced example:

```yaml
script:
  - pipe: ffischer/diawi-pipe:0.1.2
    variables:
      NAME: "foobar"
      DEBUG: "true"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by ffischer1984@googlemail.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

[![paypal](https://www.paypalobjects.com/en_US/DK/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EN22Z95HKGD74&source=url)

## cheatsheet for me:
create a new version:
* semversioner add-change --type patch --description "curl: hide progress bar with -s."
* git commit -am && git push
