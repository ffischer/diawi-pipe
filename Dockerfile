FROM alpine:3.9

RUN apk add --update --no-cache curl bash
COPY pipe /
COPY LICENSE.txt pipe.yml README.md example.apk /
RUN wget -P / https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
