#!/usr/bin/env bats
# ia1KHq14uuuQH0TEmDlZFkNagWoYE8WMSjvM68idrp

export DIAWI_TOKEN="mytest"
export CALLBACK_EMAIL="myemail"
export FILE="./example.apk"
setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/diawi-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}



@test "test file validation" {
    run docker run \
        -e FILE="example.apk" \
        -e DIAWI_TOKEN="ia1KHq14uuuQH0TEmDlZFkNagWoYE8WMSjvM68idrp" \
        -e CALLBACK_EMAIL="diawibitbucketpipe@trash-mail.com" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    #[[ "$output" = *"FILE: FILE path to android artifact is missing."* ]]
    [ "$status" -eq 1 ]
}

@test "test diawi-token validation" {
    run docker run \
        -e FILE="./blub.apk" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" = *"DIAWI_TOKEN: DIAWI_TOKEN is missing."* ]]
    [ "$status" -eq 1 ]

}

@test "test callback_email validation (is set?)" {
    run docker run \
        -e FILE="./blub.apk" \
        -e DIAWI_TOKEN="adsf" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" = *"CALLBACK_EMAIL: CALLBACK_EMAIL is missing"* ]]
    [ "$status" -eq 1 ]

}

@test "test callback_email validation (it's not a valid mail - contains no @)" {
    run docker run \
        -e FILE="./example.apk" \
        -e DIAWI_TOKEN="adsf" \
        -e CALLBACK_EMAIL="balbal" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [[ "$output" = *"balbal --> It's not a valid email address"* ]]
    [ "$status" -eq 1 ]

}
