# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: curl: hide progress bar with -s.
- patch: curl: hide progress bar with -s. Do we really need a bp-02 commit?

## 0.1.1

- patch: it works!

## 0.1.0

- minor: Initial release

